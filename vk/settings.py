# -*- coding: utf-8 -*-
confirmation_key = "282becee"
group_id = 167771039
secret_key = 'future_coming'
access_token = 'c26ac143a9c35b36f4fe020f4ba151d29de67f4cd14e8baf44e3bee26e09155bfea941a00148febdd455f'
admin_link = "https://vk.com/egorknyazev"
admin_id = "136481387"

links = {
    "Telegram": "https://tsar-bot.ru/telegram",
}

texts = {
    "welcome": "Добро пожаловать в официального бота компании TSAR-bot.\n"
               "В нём Вы можете узнать потенциал ботов для ВКонтакте и "
               "заказать бесплатное создание собственного бота.",
    "contacts": "Узнать больше Вы можете на нашем сайте tsar-bot.ru\n"
                "Связаться с нами можно по следующим контактам:\n"
                "+7(982) 277-32-47\n"
                "support@tsar-bot.ru\n"
                "Также протестируйте наших ботов в других мессенджерах, "
                "социальных сетях и голосовых ассистентах: \n"
                "{}".format("\n".join(["{}: {}".format(name, link) for name, link in links.items()])),
    "order_creating": "Проконсультироваться и заказать бесплатное создание бота можно написав контакту ниже."
                      "\n&#8195;\n{}\n&#8195;\n"
                      "Также Вы можете заказать обратный звонок, для этого в ответ на это сообщение"
                      " отправьте Ваш номер телефона".format(admin_link),
    "after order": "Поздравляем! Вы сделали первый шаг навтречу цифровому будущему. "
                   "Мы перезвоним вам в течение 15 минут.",
    "potential": 'Для демонстрации возможностей ботов вконтакте мы подготовили небольшую викторину для вас. \n'
                 'Отвечайте правильно на вопросы с помощью клавиатуры, '
                 'в конце мы подсчитаем ваше время и количество неправильных ответов, '
                 'а также напишем правильные ответы. \n'
                 'Для старта таймера нажмите "Готов!"',
    'quest_1': "Правда ли то, что первый чемпионат VK Cup был проведён 16 июля в 2012 году?",
    'quest_2': "Правда ли то, что хэштег создал Алан Тьюринг?",
    'quest_3': "Кто на фотографии?",
    'quest_4': "Из какого фильма этот кадр?",
    'result': 'Поздравляем! Вы закончили викторину за {:.1f} сек.\n'
              'Количество неправильных ответов: {}\n'
              '&#8195;\n'
              "Правильные ответы:\n"
              "Вопрос 1: Правда\n"
              "Вопрос 2: Ложь\n"
              "Вопрос 3: Андрей Рогозов - исполнительный директор ВКонтакте\n"
              "Вопрос 4: Это кадр из фильма Стив Джобс\n"
              '&#8195;\n'
              'Конечно это не все возможности доступные боту, '
              'Вы можете дать доступ ко всем событиям происходящим в сообществе, от подписки до комментария к записи\n'
              '&#8195;\n'
              'Также вы можете пройти тесты в других наших ботах, их список представлен во вкладке "Контакты"',
    'no_handler': 'Не понимаю о чём вы. Если что-то пошло не так, просто введите команду "Начать"'
}
