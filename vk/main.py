import time

from vk import vk_bot, settings, kbs
from vk.settings import texts

bot = vk_bot.Bot(confirmation_key=settings.confirmation_key,
                 access_token=settings.access_token,
                 secret_key=settings.secret_key,
                 group_id=settings.group_id)


@bot.message_handler(regexp="Начать")
@bot.payload_handler(regexp='start')
def start(msg):
    bot.send_message(msg.user_id, texts['welcome'], keyboard=kbs.start())


@bot.payload_handler(regexp="contacts")
def contacts(msg):
    bot.send_message(msg.user_id, texts['contacts'], keyboard=kbs.start())


@bot.payload_handler(regexp="order creating")
def order_creating(msg):
    bot.send_message(msg.user_id, texts['order_creating'], keyboard=kbs.order_kb())
    bot.register_next_step_handler(user_id=msg.user_id, call=process_phone_number)


def process_phone_number(msg):
    if msg.text == "Назад":
        start(msg)
        return
    bot.send_message(
        settings.admin_id,
        "Новая заявка на перезвон от vk.com/{} \nтелефон: {}".format(bot.get_user(msg.user_id, 'domain')['domain'],
                                                                     msg.text))
    bot.send_message(msg.user_id, texts['after order'], keyboard=kbs.start())


@bot.payload_handler(regexp='potential')
def potential(msg):
    bot.send_message(msg.user_id, texts['potential'], keyboard=kbs.ready())


@bot.payload_handler(func=lambda msg: msg.payload.get('back'))
def back(msg):
    if msg.payload['back'] == 'menu':
        start(msg)


users_in_quest = {}


@bot.payload_handler(func=lambda msg: msg.payload.get("question"))
def quest(msg):
    if msg.payload['question'] == '0':
        bot.send_message(msg.user_id, texts["quest_1"], kbs.quest(1))
        users_in_quest[msg.user_id] = {
            "question": 1,
            "time_start": time.time(),
            "mistakes": 0,
        }
    else:
        try:
            n = users_in_quest[msg.user_id]['question']
        except KeyError:
            return
        if not msg.payload['answer']:
            users_in_quest[msg.user_id]['mistakes'] += 1
        if n == 4:  # todo: реализовать исправление ошибки
            result_time = time.time() - users_in_quest[msg.user_id]['time_start']
            bot.send_message(msg.user_id, texts['result'].format(
                result_time,
                users_in_quest[msg.user_id]['mistakes']
            ), keyboard=kbs.start())
            users_in_quest.pop(msg.user_id)
        else:
            attachment = None
            if n == 2:
                attachment = 456239019
            elif n == 3:
                attachment = 456239020
            bot.send_message(msg.user_id, texts['quest_{}'.format(n + 1)], kbs.quest(n + 1), photo=attachment)
            users_in_quest[msg.user_id]['question'] += 1


@bot.message_handler(regexp="test")
def test(msg):
    bot.send_message(msg.user_id, "test", keyboard=kbs.my_zero_kb())


@bot.message_handler(func=lambda msg: not msg.processed)
def no_handler(msg: vk_bot.Message):
    if msg.message_id <= 1:
        start(msg)
    bot.send_message(msg.user_id, texts['no_handler'])
