# -*- coding: utf-8 -*-
from vk.vk_bot.vk_types import Keyboard, Button, Color


def my_zero_kb():
    kb = Keyboard(row_width=1)
    kb.add(
        Button(text="Red", color=Color.Red, payload={"tudim": "sudim"}),  #
    )
    return kb


def start():
    kb = Keyboard(row_width=1)
    kb.add(
        Button(text="Протестировать возможности ботов", color=Color.Blue, payload='potential'),
        Button(text="Заказать создание бота", color=Color.Blue, payload='order creating'),
        # Button(text="Оплатить подписку (in develop)", color=Color.Blue, payload='subscription'),
        Button(text="Контакты", color=Color.White, payload='contacts'),
    )
    return kb


def order_kb():
    kb = Keyboard(row_width=1, one_time=True)
    kb.add(
        Button(text="Назад", payload={"back": "menu"})
    )
    return kb


def ready():
    kb = Keyboard(row_width=1)
    kb.add(
        Button(text="Готов!", color=Color.Blue, payload={"question": '0'}),
        Button(text="Назад", color=Color.White, payload={"back": "menu"}),
    )
    return kb


def quest(ques_n):
    kb = Keyboard()
    if ques_n == 1:
        kb.row(
            Button("Правда", payload={"question": '1', 'answer': True}, color=Color.Green),
            Button("Ложь", payload={"question": '1', 'answer': False}, color=Color.Red)
        )
    elif ques_n == 2:
        kb.row_width = 4
        kb.add(
            Button("Правда", payload={"question": '2', 'answer': False}, color=Color.Green),
            Button("Ложь", payload={"question": '2', 'answer': True}, color=Color.Red),

            # Button("12", payload={"question": '2', 'answer': False}, color=Color.Blue),
            # Button("20", payload={"question": '2', 'answer': False}, color=Color.Blue),
            # Button("24", payload={"question": '2', 'answer': False}, color=Color.Blue),
            # Button("40", payload={"question": '2', 'answer': True}, color=Color.Blue),
            #
            # Button("100", payload={"question": '2', 'answer': False}, color=Color.Blue),
            # Button("тыщу", payload={"question": '2', 'answer': False}, color=Color.Blue),
            # Button("&#127819;", payload={"question": '2', 'answer': False}, color=Color.Blue),
            # Button("∞", payload={"question": '2', 'answer': False}, color=Color.Blue),
        )
    elif ques_n == 3:
        kb.row(
            Button("Аркадий Волож", payload={"question": '3', 'answer': False}, color=Color.Blue),
            Button("Илья Сегалович", payload={"question": '3', 'answer': False}, color=Color.Blue),
        )
        kb.row(
            Button("Андрей Рогозов", payload={"question": '3', 'answer': True}, color=Color.Blue),
            Button("Дмитрий Гришин", payload={"question": '3', 'answer': False}, color=Color.Blue),
        )
    elif ques_n == 4:
        kb.row(
            Button("Стив Джобс", payload={"question": '4', 'answer': True}, color=Color.Blue),
            Button("Пираты Силиконовой долины", payload={"question": '4', 'answer': False}, color=Color.Blue),
        )
        kb.row(
            Button("Американцы", payload={"question": '4', 'answer': False}, color=Color.Blue),
            Button("Игра в имитацию", payload={"question": '4', 'answer': False}, color=Color.Blue),
        )
    return kb
