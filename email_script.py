import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import csv


SUBJECT = "Test email from Python"
TO = set()
FROM = "info@tsar-bot.ru"
email_text = "Python 3.4 rules them all!"


with open("../shorten_peter.csv", 'r', newline="", encoding='utf-8') as file:
    reader = csv.reader(file)
    counter = 0
    for row in reader:
        if row[0] == 'Медицина / Здоровье / Красота':
            TO.add(row[2])
            counter += 1

TO = list(TO)
TO.append('knyazevegor012@gmail.com')

with open('../sent_email.csv', 'w', newline="", encoding='utf-8') as file:
    writer = csv.DictWriter(file, fieldnames=['email'])
    for row in TO:
        writer.writerow({'email': row})


email_text = "Если сообщение отображается не корректно перейдите по ссылке https://tsar-bot.ru/letter"
email_html = ""
with open("/home/TSAR/templates/letter.html", 'r', encoding='utf-8') as file:
    email_html = file.read()

part1 = MIMEText(email_text, 'plain')
part2 = MIMEText(email_html, 'html')


sender = smtplib.SMTP('smtp.yandex.ru', 587)
sender.starttls()
sender.login('info@tsar-bot.ru', 'VR_is_futur3')
for to in TO:
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Небольшой вопрос про чат-бота"
    msg['From'] = FROM
    msg['To'] = to
    msg.attach(part1)
    msg.attach(part2)
    sender.sendmail(FROM, to, msg.as_string())
sender.quit()
