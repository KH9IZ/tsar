import logging
import logging.config
import time

import telebot

import telegram.markups as markups
from telegram.config import *

bot = telebot.TeleBot(API_TOKEN, threaded=False)

# bot.remove_webhook()
time.sleep(0.1)
bot.set_webhook(url= WEBHOOK_BASE + WEBHOOK_PATH)

logging.config.fileConfig("./logs/logging.conf")
logger = logging.getLogger('root')

# bot.enable_save_next_step_handlers(delay=60)
# bot.load_next_step_handlers()


# TODO: Запросы контакта/местоположения
# TODO: Изменения сообщения

# TODO: Написать в контакты других ботов

@bot.message_handler(commands=['start', 'home'])
def send_welcome(msg: telebot.types.Message):
    if len(msg.text.split(" ")) > 1:
        ref_log = logging.getLogger("telegram")
        ref_log.info(f"User {msg.from_user.id} came from {msg.text.split(' ')[1]}")
    try:
        bot.send_message(msg.from_user.id, texts['welcome'], reply_markup=markups.start(), parse_mode="markdown")
    except telebot.apihelper.ApiException:
        telebot.logger.error("User who blocked bot try to start it")


@bot.message_handler(commands=['contacts'])
@bot.message_handler(regexp="Контакты")
def contacts(msg):
    bot.send_message(msg.chat.id, texts['contacts'], reply_markup=markups.contacts(), parse_mode="markdown")


@bot.message_handler(commands=['order'])
@bot.message_handler(regexp="Заказать создание бота")
def order(msg):
    sent = bot.send_message(msg.from_user.id, texts['order creating'],
                            disable_notification=True, reply_markup=markups.callback(), parse_mode="markdown")
    try:
        bot.send_contact(msg.from_user.id, *CONTACT)
    except telebot.apihelper.ApiException:
        pass
    bot.register_next_step_handler(sent, callback)


def callback(msg: telebot.types.Message):
    if msg.text == "Назад":
        send_welcome(msg)
    else:
        bot.send_message(msg.chat.id, texts['after order'], reply_markup=markups.start(), parse_mode="markdown")
        bot.send_message(admin, "Новая заявка на перезвон от:")
        bot.forward_message(admin, msg.chat.id, msg.message_id)


@bot.message_handler(regexp="Протестировать возможности ботов")
def potential(msg):
    sent = bot.send_message(msg.from_user.id, texts['potential'], reply_markup=markups.ready_for_answers(),
                            parse_mode="markdown")
    bot.register_next_step_handler(sent, quest)


@bot.message_handler(content_types=['photo'])
def test(msg: telebot.types.Message):
    print(msg.photo[0])


users_in_quest = {}


def quest(msg):
    if msg.text == "Назад":
        send_welcome(msg)
        return
    uid = msg.from_user.id
    if uid not in users_in_quest.keys():
        users_in_quest[uid] = {
            "question": 1,
            "time_start": time.time(),
            "mistakes": 0,
        }
    n = users_in_quest[uid]['question']
    if n == 1:
        sent = bot.send_photo(uid, "AgADAgAD7KkxG5BZOEkkjaAoG8_7cwT69A4ABMlSV9Bdab5b6k8DAAEC", texts['answer 1'],
                              reply_markup=markups.answer_1(), parse_mode="markdown")
        users_in_quest[uid]['question'] += 1
        bot.register_next_step_handler(sent, quest)
    if n == 2:
        if "Все" in msg.text:
            txt = "Верно!"
        else:
            txt = "Вы явно недооцениваете этих умных помощников.\n" \
                  "Боты поддерживают все доступные на данный момент форматы сообщений."
            users_in_quest[uid]['mistakes'] += 1
        bot.send_message(uid, txt, reply_markup=telebot.types.ReplyKeyboardRemove())
        bot.send_message(uid, texts['answer 2'], reply_markup=markups.answer_2(), parse_mode="markdown")
        users_in_quest[uid]["question"] += 1


@bot.callback_query_handler(func=lambda c: users_in_quest[c.from_user.id]['question'] == 3)
def answer_3(c: telebot.types.CallbackQuery):
    uid = c.from_user.id
    if c.data == "True":
        cq = "Привильно!"
    else:
        cq = "Нет, бот не может начать беседу с человеком, который ни разу его не активировал."
        users_in_quest[uid]["mistakes"] += 1
    bot.answer_callback_query(c.id, cq, show_alert=True)
    bot.edit_message_text(texts['answer 2'] + "\n\nПравильный ответ: Написать человеку первым",
                          uid, c.message.message_id, parse_mode="markdown")
    sent = bot.send_message(uid, texts['answer 3'], reply_markup=markups.query(), parse_mode="markdown")
    users_in_quest[uid]["question"] += 1
    bot.register_next_step_handler(sent, question_4)


# TODO: fix by get
@bot.inline_handler(
    func=lambda q: users_in_quest.get(q.from_user.id) and users_in_quest[q.from_user.id]["question"] == 4)
def question_3(q: telebot.types.InlineQuery):
    bot.answer_inline_query(q.id, markups.answer_3(), cache_time=5)


def question_4(msg):
    uid = msg.from_user.id
    if "Бесплатно" in msg.text:
        bot.send_message(uid, "Всё верно! Ни копеки за создание, вы платите лишь за ежемесячную подписку.")
    else:
        bot.send_message(uid, "Не верно. За создание бота вы не платите совсем, "
                              "оплата происодит лишь за ежемесячную подписку")
        users_in_quest[uid]['mistakes'] += 1
    bot.send_message(uid, texts['answer 4'], reply_markup=markups.answer_4(), parse_mode="markdown")


@bot.callback_query_handler(
    func=lambda q: users_in_quest.get(q.from_user.id) and users_in_quest[q.from_user.id]["question"] == 4)
def end_quest(q):
    uid = q.from_user.id
    result = users_in_quest.pop(uid)
    # Выводим ответ одновременно выствляя главную клавиатуру
    bot.send_message(uid, "Ответ: `800` *рублей в месяц*", parse_mode="markdown", reply_markup=markups.start(),
                     disable_notification=True)
    # Убираем клавиатуру к последнему вопросу
    bot.edit_message_reply_markup(uid, q.message.message_id, reply_markup=telebot.types.InlineKeyboardMarkup())
    # Выводим результаты показывая кнопку "Поделиться"
    bot.send_message(q.from_user.id,
                     texts['result'].format(int(time.time() - result["time_start"]), result["mistakes"]),
                     reply_markup=markups.promo(), parse_mode="markdown")
    bot.answer_callback_query(q.id, "Правильный ответ!")


@bot.inline_handler(func=lambda q: True)
def answer_3(q: telebot.types.InlineQuery):
    bot.answer_inline_query(q.id, markups.promo_inline(), cache_time=5)


@bot.message_handler(func=lambda msg: True)
def no_handler(msg):
    bot.send_message(msg.from_user.id, texts['no_handler'])


def telegram_process(request):
    if request.headers.get('content-type') == 'application/json':
        json_string = request.get_data().decode('utf-8')
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return ''
    else:
        return "err"


if __name__ == "__main__":
    bot.remove_webhook()
    bot.enable_save_next_step_handlers(delay=60)
    bot.load_next_step_handlers()
    bot.infinity_polling()
