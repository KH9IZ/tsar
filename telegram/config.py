TEL = "+7(982)277-32-47"
EMAIL_ORDER = "order@tsar-bot.ru"
EMAIL_SUPPORT = "support@tsar-bot.ru"
SITE = "https://tsar-bot.ml"
USERNAME = "@TSAR_bot_ru"

CONTACT = ("+79822773247", "TSAR")  # (phone, first_name, [second_name])

links = {
    "ВКонтакте": "https://vk.com/im?sel=-167771039",
}

admin = 256711367  # knyazevegor01@mail.ru
API_TOKEN = '563053943:AAG2D8EdWoNSsz0ZvWyrWlyzcGkZG_k5lmQ'
WEBHOOK_HOST = "tsar-bot.ml"
WEBHOOK_PORT = 443
WEBHOOK_SSL_CERT = "/etc/letsencrypt/archive/tsar-bot.ml/fullchain1.pem"
WEBHOOK_SSL_PRIV = "/etc/letsencrypt/live/tsar-bot.ml/privkey.pem"
WEBHOOK_BASE = f"https://{WEBHOOK_HOST}:{WEBHOOK_PORT}"
WEBHOOK_PATH = "/telegram/"


answer_photo_1 = 'AgADAgADTakxGwIrkEseW9tvYSD91bbMtw4ABN0FKQ0CvViqWR4CAAEC'

texts = {
    'welcome': "Добро пожаловать в официального бота компании *TSAR-bot*.\n\n"
               "В нём Вы можете:\n "
               "• Узнать _потенциал_ ботов `Telegram`, \n"
               "• Заказать *бесплатное* создание собственного бота.",
    'contacts': "*Контакты*\n\n"
                "Узнать больше Вы можете на нашем сайте tsar-bot.ru\n"
                "Связаться с нами можно по следующим контактам:\n"
                "• +7(982) 277-32-47\n"
                "• support@tsar-bot.ru\n\n"
                "Также протестируйте наших ботов в других мессенджерах, "
                "социальных сетях и голосовых ассистентах: \n"
                "{}".format("\n".join(["*{}*: {}".format(name, link) for name, link in links.items()])),
    'order creating': "*Заказ бота* \n\n"
                      "Чтобы составить технический заказ \n"
                      "_оставьте_ свой контакт по кнопке на клавиатуре \n"
                      "или \n"
                      "_отправьте_ другой контакт в ответном сообщении\n"
                      "или \n"
                      "_напишите_ контакту ниже \n",
    'after order': "*Принято!* \n\n"
                   "Наш менеджер напишет вам через несколько минут. \n"
                   "А пока вы можете протестировать возможности телеграм ботов в соответствующем меню.",
    "potential": "*Викторнина*\n\n"
                 "Для демонстрации возможностей _ботов Telegram_ мы подготовили небольшую викторину. \n"
                 "4 вопроса, таймер, мы готовы! А вы?",
    'answer 1': "Какие типы сообщений кроме текстовых способны *принимать* и *отправлять* боты?",
    'answer 2': "Что из перечисленного *не могут* Telegram боты?",
    'answer 3': "Сколько стоит разработка бота в *TSAR bot*?",
    'answer 4': "Во сколько рублей в месяц вам обойдётся подписка, в которую входит:*\n "
                "• Круглосуточная техническая поддержка\n "
                "• Хостинг\n"
                "• Дополнение функционала бота* и другое?",
    'result': 'Поздравляем! Вы закончили викторину за *{} сек*.\n'
              'Количество неправильных ответов: *{}*\n'
              'Вы можете пройти тесты в других наших ботах, их список представлен во вкладке /contacts',
    'no_handler': "Не понимаю о чём вы. Если что-то пошло не так, просто введите команду /home",
}
