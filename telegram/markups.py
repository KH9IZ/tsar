from telebot.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton, \
    InlineQueryResultArticle, InputTextMessageContent

from telegram.config import SITE, links


def start():
    mp = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    mp.add(
        KeyboardButton("Протестировать возможности ботов"),
        KeyboardButton("Заказать создание бота"),
        KeyboardButton("Контакты"),
    )
    return mp


def callback():
    mp = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1, one_time_keyboard=True)
    mp.add(
        KeyboardButton("Отправить контакт", request_contact=True),
        KeyboardButton("Назад"),
    )
    return mp


def contacts():
    mp = InlineKeyboardMarkup(row_width=1)
    mp.add(
        InlineKeyboardButton("tsar-bot.ru", url=SITE),
        *(InlineKeyboardButton(link[0], url=link[1]) for link in links.items())
    )
    return mp


def ready_for_answers():
    mp = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    mp.add(
        KeyboardButton("Готов!"),
        KeyboardButton("Назад")
    )
    return mp


def answer_1():
    mp = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    mp.row(
        KeyboardButton("Фото"),
        KeyboardButton("Стикеры"),
    )
    mp.add(
        KeyboardButton("Голосовые сообщения"),
        KeyboardButton("Все доступные обычным пользоватаелям"),
    )
    return mp


def answer_2():
    mp = InlineKeyboardMarkup(row_width=1)
    mp.add(
        InlineKeyboardButton("Написать человеку первым", callback_data="True"),
        InlineKeyboardButton("Принять оплату через онлайн кассу", callback_data="False"),
        InlineKeyboardButton("Выложить пост в канал", callback_data="False"),
        InlineKeyboardButton("Разослать рекламное сообщение",
                             callback_data="False"),
    )
    return mp


def answer_3():
    descs = {
        "0 рублей": "Бесплатно. Для старта разработки небходимо оплатить первые 2 месяца работы бота.",
        "1 000 рублей": "Все исходники передаются вам, мы не занимаемся содержанием бота.",
        "50 000 рублей": "+ содержание бота бесплатно",
        "Договорная": "Цена составляется индивидуально для каждого проекта",
    }
    mp = [InlineQueryResultArticle(id=i + 1,
                                   title=j[0],
                                   description=j[1],
                                   input_message_content=InputTextMessageContent(j[1]))
          for i, j in enumerate(descs.items())]
    return mp


def answer_4():
    mp = InlineKeyboardMarkup(row_width=1)
    mp.add(
        InlineKeyboardButton("800 рублей", callback_data="True"),
        InlineKeyboardButton("800 рублей в месяц", callback_data="True"),
        InlineKeyboardButton("800 р/мес", callback_data="True"),
        InlineKeyboardButton("800 р.", callback_data="True"),
    )
    return mp


def query():
    mp = InlineKeyboardMarkup()
    mp.add(
        InlineKeyboardButton("Ответить", switch_inline_query_current_chat="")
    )
    return mp


def promo():
    mp = InlineKeyboardMarkup()
    mp.add(
        InlineKeyboardButton("Посоветовать бота друзьям", switch_inline_query="")
    )
    return mp


def promo_button():
    mp = InlineKeyboardMarkup()
    mp.add(
        InlineKeyboardButton("Перейти к боту", url="t.me/ru_tsar_bot?start=promotion")
    )
    return mp


def promo_inline(text=None):
    txt = ("Привет! Посмотри какого интересного бота сделали парни из {}",
           "Приветствую. Пишу чтобы обратить ваше внимание на компанию {}, "
           "производящую ботов для мессенджеров и социальных сетей.\n Оцените качество их ботов по кнопке ниже")
    friend = InlineQueryResultArticle(id=1,
                                      title="Другу",
                                      input_message_content=InputTextMessageContent(txt[0].format("*TSAR*"),
                                                                                    parse_mode="markdown"),
                                      reply_markup=promo_button(),
                                      description=txt[0].format("TSAR"))
    colleague = InlineQueryResultArticle(id=2,
                                         title="Коллеге",
                                         input_message_content=InputTextMessageContent(txt[1].format("*TSAR*"),
                                                                                       parse_mode="markdown"),
                                         reply_markup=promo_button(),
                                         description=txt[1].format("TSAR"))
    return [friend, colleague]
