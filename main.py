import logging
import logging.config

from flask import Flask, render_template, redirect, request

from telegram.main import telegram_process
from vk.main import bot as vk_bot


app = Flask(__name__)
logging.config.fileConfig("./logs/logging.conf")


@app.route('/')
@app.route('/index')
def index():
    if request.args.get('from'):
        refer_log = logging.getLogger("site")
        refer_log.info(f"User followed the link from {request.args['from']}")
    return render_template("index.html")


@app.route('/letter')
def letter():
    return render_template('letter.html')


@app.route('/usage')
def usage():
    if request.args.get('from') == 'email':
        vk_bot.send_message(136481387, f"Кто-то зашёл на сайт usage через email")
    return render_template('usage.html')


@app.route('/get_order', methods=['POST'])
def get_order():
    s = '\n'.join((f"{item[0]}: {item[1]}" for item in request.form.items()))
    vk_bot.send_message(136481387, f"Новая заявка с сайта:\n{s}")
    return redirect('https://tsar-bot.ml/#contact')


@app.route('/vk', methods=['POST'])
def processing():
    return vk_bot.process_request(request)


@app.route('/telegram/', methods=["POST", "GET"])
def telegram():
    if request.method == "GET":
        return redirect("tg://resolve?domain=ru_tsar_bot&start=vk")
    else:
        pass
        return telegram_process(request)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
